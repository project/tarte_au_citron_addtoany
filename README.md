# Tarte au citron Add To Any
Submodule of [Tarte au citron](https://www.drupal.org/project/tarte_au_citron) module.

Comply to the European cookie law using [tarteaucitron.js](https://github.com/AmauriC/tarteaucitron.js) with [Add To Any](https://www.drupal.org/project/addtoany) module.

## Installation

1. Install Tarte au citron and Add To Any modules
2. Download the module and enable it
4. Configure at Administer > Configuration >
Tarte au citron > Settings for Tarte au citron module.


### Composer installation
1. Add the module
```bash
composer require drupal/tarte_au_citron_addtoany
